package com.kesizo.microservices.subscribe.stream;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.IntegrationComponentScan;

@Configuration
@EnableBinding(ItemSink.class)
@IntegrationComponentScan
public class IntegrationConfiguration {

}
