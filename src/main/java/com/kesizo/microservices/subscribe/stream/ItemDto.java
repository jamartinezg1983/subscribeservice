package com.kesizo.microservices.subscribe.stream;

import java.io.Serializable;

public class ItemDto implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = -3080691138379491265L;
	
	private String message;
 
    public ItemDto(){
        //Para Jackson
    }
 
    public ItemDto(String message) {
        this.message = message;
    }
    
    public String getMessage() {
		return message;
	}
 
    public void setMessage(String message) {
		this.message = message;
	}

	@Override
    public String toString() {
        return "ItemDto{" +
                "message='" + message + "'" + '}';
    }

}
